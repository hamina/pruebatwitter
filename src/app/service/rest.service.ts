import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RestService {
  private objeto={
    public_key:"6664f225ace4a2a1844f1538426ee8294441ba83",
    objeto:{}
  };
  private numberFormat = new Intl.NumberFormat('en-US', { style: 'currency', currency: 'COP' });
  private url:string="https://server.worldgreatestdancers.com/";
  constructor(private httpClient:HttpClient) { }

  register(objeto){
    this.objeto.objeto=objeto;
    return this.httpClient.post<any>(this.url+"welcome/setParticipant",this.objeto).toPromise();
  }

  login(objeto){
    this.objeto.objeto=objeto;
    return this.httpClient.post<any>(this.url+"welcome/login",this.objeto).toPromise();
  }
  loginJudge(objeto){
    this.objeto.objeto=objeto;
    return this.httpClient.post<any>(this.url+"welcome/loginJudge",this.objeto).toPromise();
  }
  getUserData(objeto){
    this.objeto.objeto=objeto;
    return this.httpClient.post<any>(this.url+"welcome/getAllOfUser",this.objeto).toPromise();
  }
  getUserByEmail(email){
    this.objeto.objeto=email;
    return this.httpClient.post<any>(this.url+"welcome/getUserByEmail",this.objeto).toPromise(); 
  }
  prueba(objeto,id){
    console.log(objeto);
    /*return this.httpClient.post(this.url+"welcome/setVideo/"+id,objeto,{ responseType : 'text', headers: {
      'Content-Type': 'multipart/form-data'
    }} ) ;*/
  }
  putaMadre(objeto){
    return this.httpClient.post<any>(this.url+"welcome/setVideo",objeto).toPromise();
  }
  getVerificationEmail(objeto){
    this.objeto.objeto=objeto;
    return this.httpClient.post<any>(this.url+"welcome/getVerificationEmail",this.objeto).toPromise();
  }
  getParticipantes(){
    this.objeto.objeto=true;
    
    return this.httpClient.post<any>(this.url+"welcome/getParticipantes",this.objeto).toPromise();
  }
  getPassword(objeto){
    this.objeto.objeto=objeto;
    return this.httpClient.post<any>(this.url+"welcome/setPassword",this.objeto).toPromise();
  }
  
}
