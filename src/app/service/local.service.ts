import { Injectable } from '@angular/core';
import { catchError, retry } from 'rxjs/operators';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
/*Servicio especifico para local storage */
export class LocalService {

  usuario: any;
  setLogout;
  checkIsLogin;

  constructor(private router: Router) { }

  getItem(nombre: string) {
    var obj = localStorage.getItem(nombre);
    if (obj) {
      return this.decode(obj);
    } else {
      return null;
    }

  }
  setItem(nombre: string, obj: any) {
    localStorage.setItem(nombre, this.code(obj));
  }
  removeItem(nombre: string) {
    localStorage.removeItem(nombre);
  }
  private code(objeto) {
    return btoa(JSON.stringify(objeto));
  }
  private decode(objeto) {
    return JSON.parse(atob(objeto));
  }

  validateLogin() {
    var usu = this.getItem('usuarioTwitter');
    
    if (!usu) {
      var linked = window.location.pathname;
      if (linked.includes("cosas que no van")) {
        
      } else {
        //this.router.navigate(['/login/world'])
        this.logout();
      }
      this.checkIsLogin ? clearTimeout(this.checkIsLogin) : null;
    } else {
      
      //esta parte es para averiguar la cantidad de horas disponibles en sesión
      var diffTime1 = Math.abs(Date.now() - usu.fecha);
      var diffDays1 = Math.ceil(diffTime1 / (1000 * 60));
      
      //son 3 horas 
      if (diffDays1 > 180) {
        //this.router.navigate(["/login/world"]);
        this.logout();
      } else {
        this.validateNavigation();
        this.checkIsLogin ? null : this.intervals();
      }
    }
  }
  login(user) {

    user.fecha=Date.now();
    this.setItem('usuarioTwitter', user);
    this.intervals();       
  }

  intervals() {
    this.checkIsLogin = setInterval(() => {
      this.validateLogin();
    }, 300000);//1000*60*5 osea cada  5 min
  }

  
  validateNavigation() {
    var toNavigate = this.setUrl();
    this.router.navigate([window.location.pathname])
    /*switch (nivel) {
      case "478":
        navigation[0].hidden = true;
        navigation[1].hidden = true;
        navigation[2].hidden = true;
        toNavigate = "/maintenance";
        break;
      case '722':
      case "435":
        navigation[0].hidden = true;
        navigation[1].hidden = true;
        navigation[2].children[1].hidden = true;
        //navigation[2][1].hidden = true;
        break;
      case '753':
      case '358':
        navigation[0].hidden = false;
        navigation[1].hidden = false;
        navigation[2].children[1].hidden = false;
        break;
    }*/
    /*toNavigate = "login";

    ;*/


  }
  setUrl() {
    if (window.location.pathname.includes("login") || window.location.pathname == "/") {
      return "/cliente";
    } else {
      return window.location.pathname;
    }
  }
  logout() {
    var usuario=this.getItem("usuarioTwitter");
    //remuevo la variable temporal
    this.removeItem('usuarioTwitter')
    //quito todos los modales

    // Register the navigation to the service
    
    //limpio el cronjob
    this.checkIsLogin ? clearTimeout(this.checkIsLogin) : null;
    //navego al inicio

    
    var navigate='/';
    if(usuario){
      if(usuario.type){
        navigate="'/'";
      }
    }
    this.router.navigate([navigate]);
    
  }
}
