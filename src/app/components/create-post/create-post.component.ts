import { Component, OnInit, Output,EventEmitter, Input } from '@angular/core';


@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.scss']
})
export class CreatePostComponent implements OnInit {
  @Output() enviarComentario = new EventEmitter();
  @Input("type") type:string;

  comentario:any={
    texto:"",
    foto:"assets/img/user.png",
    likes:[{id:"1"},{id:"2"},{id:"3"}],
    comentario:[]
  }
  constructor() { }
  

  ngOnInit(): void {
  }
  enviar(){
    var comentario=this.comentario
    this.comentario={
      texto:"",
      foto:"assets/img/user.png",
      likes:[{id:"1"},{id:"2"},{id:"3"}],
      comentario:[]
    }
    this.enviarComentario.emit(comentario);
  }

}
