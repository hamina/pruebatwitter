import { Component, OnInit } from '@angular/core';
import { FileService } from 'src/app/service/file.service';
import { AppToastService } from 'src/app/service/app-toast.service';
import { LocalService } from 'src/app/service/local.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.scss']
})
export class PerfilComponent implements OnInit {
  usuario:any={email:"",password:"",nombre:"",apellido:"",fecha_nacimiento:""}
  image:any="assets/img/user.png"
  constructor(private fileService:FileService,private toast:AppToastService,
    private local:LocalService) { }

  ngOnInit(): void {
    this.usuario=this.local.getItem("usuarioTwitter");
  }
  modificar(){

  }
  initFileUpload() {
    document.getElementById("file").click();
  }
  uploadFile(doomEvent) {
    this.fileService.uploadDataUrl(doomEvent.item(0)).subscribe(data => {
      var extension = data.split(";")[0].split("/")[1];
      console.log(data);
      this.image=data;
      //this.clienteService.uplaodImageEvent(this.idEventImage, data.split(",")[1], extension).subscribe(response => {
        this.toast.show("Imágen guarda", "", "bg-success");

    }, err => {
      this.toast.show("OPS!! ocurrió un error", "OK", "bg-danger");
    });
  }
}
