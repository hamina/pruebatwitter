import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {
  
  comentar=
  {
    texto:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer in dapibus lorem. Sed sed enim sit amet ex volutpat auctor vel vitae quam. Phasellus consectetur dictum enim, blandit tempus nulla mattis ultricies. Aenean pretium, risus a tincidunt aliquet, mi dui dapibus dolor, quis congue turpis felis sed velit. Sed felis ligula, tincidunt porttitor venenatis vitae, commodo ac ipsum. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed et dapibus mi, ac sodales dolor.",
    foto:"assets/img/user.png",
    likes:[{id:"1"},{id:"2"},{id:"3"}],
    comentario:[
      {texto:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer in dapibus lorem. Sed sed enim sit amet ex volutpat auctor vel vitae quam. Phasellus consectetur dictum enim, blandit tempus nulla mattis ultricies. Aenean pretium, risus a tincidunt aliquet, mi dui dapibus dolor, quis congue turpis felis sed velit. Sed felis ligula, tincidunt porttitor venenatis vitae, commodo ac ipsum. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed et dapibus mi, ac sodales dolor.",
    foto:"assets/img/user.png",
    likes:[{id:"1"},{id:"2"},{id:"3"}]},
    {texto:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer in dapibus lorem. Sed sed enim sit amet ex volutpat auctor vel vitae quam. Phasellus consectetur dictum enim, blandit tempus nulla mattis ultricies. Aenean pretium, risus a tincidunt aliquet, mi dui dapibus dolor, quis congue turpis felis sed velit. Sed felis ligula, tincidunt porttitor venenatis vitae, commodo ac ipsum. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed et dapibus mi, ac sodales dolor.",
    foto:"assets/img/user.png",
    likes:[{id:"1"},{id:"2"},{id:"3"}],
    comentario:[
      {texto:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer in dapibus lorem. Sed sed enim sit amet ex volutpat auctor vel vitae quam. Phasellus consectetur dictum enim, blandit tempus nulla mattis ultricies. Aenean pretium, risus a tincidunt aliquet, mi dui dapibus dolor, quis congue turpis felis sed velit. Sed felis ligula, tincidunt porttitor venenatis vitae, commodo ac ipsum. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed et dapibus mi, ac sodales dolor.",
    foto:"assets/img/user.png",
    likes:[{id:"1"},{id:"2"},{id:"3"}]}
    
    ]},
    {texto:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer in dapibus lorem. Sed sed enim sit amet ex volutpat auctor vel vitae quam. Phasellus consectetur dictum enim, blandit tempus nulla mattis ultricies. Aenean pretium, risus a tincidunt aliquet, mi dui dapibus dolor, quis congue turpis felis sed velit. Sed felis ligula, tincidunt porttitor venenatis vitae, commodo ac ipsum. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed et dapibus mi, ac sodales dolor.",
    foto:"assets/img/user.png",
    likes:[{id:"1"},{id:"2"},{id:"3"}]}
    ]
  }
  comentarios:any[]=[];
  constructor() { }

  ngOnInit(): void {
  }
  cogerComentario(comentario){
    this.comentarios.push(comentario);
    console.log(comentario);
    //this.enviarComentario.emit(this.comentario);
  }
}
