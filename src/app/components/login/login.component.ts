import { Component, OnInit, AfterViewInit } from '@angular/core';
import { LocalService } from 'src/app/service/local.service';
import { AppToastService } from 'src/app/service/app-toast.service';
import { Router } from '@angular/router';
import { FileService } from 'src/app/service/file.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit,AfterViewInit {
  isSign:boolean=true;
  usuario:any={email:"",password:"",nombre:"",apellido:"",fecha_nacimiento:""}
  repeatPassword:any="";
  usuarios:any=[
    {email:"Jalegria@gmail.com",password:"1",nombre:"Jenifer ",apellido:"Alegría Diaz",fecha_nacimiento:"1985-09-5"},
    {email:"Sapelo@hotmail.co",password:"2",nombre:"Sandra",apellido:"Pérez Lopez",fecha_nacimiento:"2000-09-8"},
    {email:"Samanta@gmail.com",password:"3",nombre:"Samanta",apellido:"Misquera",fecha_nacimiento:"1985-10-2"}
  ]
  constructor(private local:LocalService,private toast:AppToastService,
    private router:Router) { }
  
  ngOnInit(): void {
  }
  ngAfterViewInit(){
    
  }

  login(){
    this.lleno();
    if(this.lleno()){
      console.log(this.usuario);
      for (const usu of this.usuarios) {
        if(this.usuario.password==usu.password && this.usuario.email==usu.email){
          this.usuario=usu;
          this.local.login(this.usuario)
          this.router.navigate(['/perfil'])
        }else{
          this.toast.show("Alerta","Usuario no encontrado","bg-danger");
        }      
      }
    }else{
      this.toast.show("Alerta","Rellene los campos","bg-danger");
    }
    
    //this.local.login(this.usuario);
    
  }
  registrar(){
    //this.lleno();
    if(this.lleno()){
      if(this.usuario.password==this.repeatPassword){
        this.usuarios.push(this.usuario);
      }else{
        this.toast.show("alerta","Las contraseñas no coinciden","bg-danger");
      }
    }else{
      this.toast.show("Alerta","Rellene los campos","bg-danger");
    }
  }
  lleno(){
    var isvacio=false;
    var usuario=this.usuario
    for (const key in usuario) {
      if(usuario[key].replace(/ /g, " ")==""){
        isvacio=true;
      }
    }
    return isvacio;
  }

  
}
