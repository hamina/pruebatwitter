import { Component, OnInit, Input } from '@angular/core';
import { LocalService } from 'src/app/service/local.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {
  @Input("comentario") comentario:any;
  usuario:any;
  ulike:boolean=false;
  isRespuesta:boolean=false;
  constructor(private local:LocalService) {
    
   }

  ngOnInit(): void {
    if(!this.comentario){
      this.comentario=
      {texto:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer in dapibus lorem. Sed sed enim sit amet ex volutpat auctor vel vitae quam. Phasellus consectetur dictum enim, blandit tempus nulla mattis ultricies. Aenean pretium, risus a tincidunt aliquet, mi dui dapibus dolor, quis congue turpis felis sed velit. Sed felis ligula, tincidunt porttitor venenatis vitae, commodo ac ipsum. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed et dapibus mi, ac sodales dolor.",
      foto:"assets/img/user.png",
      likes:[{id:"1"},{id:"2"},{id:"3"}],
      comentario:[]}
    }else{
      console.log("holita");
      console.log(this.comentario);
    }
    this.usuario=this.local.getItem("usuarioTwitter");
    this.usuario.id="4";
    for (let index = 0; index < this.comentario.likes.length; index++) {
      if(this.comentario.likes[index].id==this.usuario.id){
        this.ulike=true;
      }
    }
  }
  setLike(){
    this.ulike=!this.ulike;
    this.ulike ? this.comentario.likes.push({id:this.usuario.id}) :this.comentario.likes.splice(this.comentario.likes.length-1,1) ;
    
    
   
  }
  cogerComentario(comment){
    this.comentario.comentario.push(comment);
  }
}
